/*
| ----------------------------------------------------------------------------------
| TABLE OF CONTENT
| ----------------------------------------------------------------------------------

-Preloader
-Datepicker
-Scale images
-Select customization
-Zoom Images
-Main slider
-Slider
-Slider with thumbnails
-Upload
-Map filters
-Chart
-Form validation
-Input effects
*/



$(document).ready(function() {

  "use strict";


/////////////////////////////////////////////////////////////////
// PRELOADER
/////////////////////////////////////////////////////////////////

    var $preloader = $('#page-preloader'),
    $spinner   = $preloader.find('.spinner-loader');
    $spinner.fadeOut();
    $preloader.delay(50).fadeOut('slow');


/////////////////////////////////////////////////////////////////
// INPUT EFFECTS
/////////////////////////////////////////////////////////////////

  (function inpEff() {
      if (!String.prototype.trim) {
          (function() {
              var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
              String.prototype.trim = function() {
                  return this.replace(rtrim, '');
              };
          })();
      }

      [].slice.call( document.querySelectorAll( '.ui-input__field' ) ).forEach( function( inputEl ) {
          if( inputEl.value.trim() !== '' ) {
              classie.add( inputEl.parentNode, 'ui-input_filled' );
          }
          inputEl.addEventListener( 'focus', onInputFocus );
          inputEl.addEventListener( 'blur', onInputBlur );
      } );

      function onInputFocus( ev ) {
          classie.add( ev.target.parentNode, 'ui-input_filled' );
      }

      function onInputBlur( ev ) {
          if( ev.target.value.trim() === '' ) {
              classie.remove( ev.target.parentNode, 'ui-input_filled' );
          }
      }
  })();

// DATEPICKER //

  if ($('.input-group.date').length) {
    $('.input-group.date').datepicker({
      language: "uk",
      daysOfWeekHighlighted: "0,6",
      todayHighlight: true
    });
  }

/////////////////////////////////////////////////////////////////
// SCALE IMAGES
/////////////////////////////////////////////////////////////////

  if ($('.img-scale').length) {
    $(function () { objectFitImages('.img-scale') });
  }


/////////////////////////////////////////////////////////////////
// SELECT CUSTOMIZATION
/////////////////////////////////////////////////////////////////

if ($('.selectpicker').length) {
   $(function() {
    var $select = $('.selectpicker').selectpicker({
      noneResultsText: "нічого не знайдено {0}"
    });

    $(':reset').on('click', function(evt) {
        evt.preventDefault();
        var $form = $(evt.target).closest('form');
        $form[0].reset();
        $form.find('select').selectpicker('render')
    });
  })
}


/////////////////////////////////////////////////////////////////
// ZOOM IMAGES
/////////////////////////////////////////////////////////////////

  if ($('.js-zoom-gallery').length) {
      $('.js-zoom-gallery').each(function() {
          $(this).magnificPopup({
              delegate: '.js-zoom-gallery__item',
              type: 'image',
              gallery: {
                enabled:true
              },
        mainClass: 'mfp-with-zoom',
        zoom: {
          enabled: true,
          duration: 300,
          easing: 'ease-in-out',
          opener: function(openerElement) {
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
          });
      });
    }


  if ($('.js-zoom-images').length) {
      $('.js-zoom-images').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom',
        zoom: {
          enabled: true,
          duration: 300,
          easing: 'ease-in-out',
          opener: function(openerElement) {
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
      });

    }


  if ($('.popup-youtube, .popup-vimeo, .popup-gmaps').length) {
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false,
      zoom: {
        enabled: true,
        duration: 300
      }
    });
  }



////////////////////////////////////////////
// MAIN SLIDER
///////////////////////////////////////////


  if ($('#main-slider').length) {

      var sliderWidth = $("#main-slider").data("slider-width");
      var sliderHeigth = $("#main-slider").data("slider-height");
      var sliderArrows = $("#main-slider").data("slider-arrows");
      var sliderButtons = $("#main-slider").data("slider-buttons");

      $( '#main-slider' ).sliderPro({
          width:  sliderWidth,
          arrows: sliderArrows,
          buttons: sliderButtons,
          autoHeight: true,
          fade: true,
          fullScreen: true,
          touchSwipe: false,
          autoplay: true,
          autoplayOnHover: "none",
          autoplayDelay: 8000
      });
  }


/////////////////////////////////////////////////////////////////
// SLIDER
/////////////////////////////////////////////////////////////////

  if ($('.js-slider').length) {
    $('.js-slider').slick();
  };


/////////////////////////////////////////////////////////////////
// SLIDER WITH THUMBNAILS
/////////////////////////////////////////////////////////////////

  if ($(".js-slider-thumb_main").length) {
     $('.js-slider-thumb_main').slick({
      fade: true,
      asNavFor: '.js-slider-thumb_nav'
    });
    $('.js-slider-thumb_nav').slick({
      slidesToScroll: 1,
      asNavFor: '.js-slider-thumb_main',
      focusOnSelect: true
    });
   }

/////////////////////////////////////////////////////////////////
// UPLOAD
/////////////////////////////////////////////////////////////////

  if ($(".js-upload").length) {
    $(".js-upload").dropzone({
      url: "../php/index.php",
      clickable: ".js-upload-btn",
      previewsContainer: ".js-upload-container",
      previewTemplate: "<div class='row form-group justify-content-between'><span class='col-auto ui-upload__name' data-dz-name></span><span class='col text-right'><a class='btn-cancel btn btn-light' href='#' data-dz-remove >Cкасувати<svg class='ic' width='24' height='24'><use xlink:href='../svg-symbols.svg#close'></use></svg></a></span></div>"
    });
  }

  /////////////////////////////////////////////////////////////////
  // Map filters
  /////////////////////////////////////////////////////////////////


  if ($(".b-map").length) {
    $( ".js-map-toggle" ).on( "click", function() {
      $(this).toggleClass('active');
      $(".b-map-filter").fadeToggle();
    })

    $( ".js-map-close" ).on( "click", function() {
      $(".js-map-toggle").toggleClass('active');
      $(".b-map-filter").fadeOut();
    })

  };


  /////////////////////////////////////////////////////////////////
  // Charts
  /////////////////////////////////////////////////////////////////

  //Bar Chart 1

  if ($("#chartBar1").length) {
    var myChart = echarts.init(document.getElementById('chartBar1'));
    var data = [8.1, 4.0, 2.9, 4.8, 7.4, 3.8, 8.1, 8.1, 8.1];
    var yMax = 10;
    var dataShadow = [];

    for (var i = 0; i < data.length; i++) {
        dataShadow.push(yMax);
    }

      // specify chart configuration item and data
      var option = {
          tooltip: {},
          xAxis: {
              data: ["Залізо","Марганець","Мідь","Нітрити","Сульфати","Хром 6+","Фосфати","Хром","БСК 5"],

              axisLabel: {
                rotate: 45,
                color: "#848e99"
              }
          },
          yAxis: {},
          series: [
            { // For shadow
                type: 'bar',
                itemStyle: {
                    normal: {color: 'rgba(0,0,0,0.05)'}
                },
                barGap:'-100%',
                barCategoryGap:'40%',
                data: dataShadow,
                animation: false
            },
            {
              type: 'bar',
              label: {
                  normal: {
                      show: true,
                      position: 'inside',
                      textStyle: {
                          color: '#000',
                          fontWeight: 700
                      }
                  }
              },
              itemStyle: {
                  normal: {
                      color: new echarts.graphic.LinearGradient(
                          0, 0, 0, 1,
                          [
                              {offset: 0, color: '#f7b018'},
                              {offset: 1, color: '#fe9529'}
                          ]
                      )
                  },
                  emphasis: {
                      color: new echarts.graphic.LinearGradient(
                          0, 0, 0, 1,
                          [
                              {offset: 0, color: '#fe9529'},
                              {offset: 1, color: '#f7b018'}
                          ]
                      )
                  }
              },
              data: data
          }
            ]
      };

      // use configuration item and data specified to show chart
      myChart.setOption(option);
   }

  //Bar Chart 2

  if ($("#chartBar2").length) {
    var myChart2 = echarts.init(document.getElementById('chartBar2'));

    // specify chart configuration item and data
    var option2 = {
        color: ['#b4ec51', '#fad961', '#3080a9', '#f5515f', '#f38500'],
        legend: {},
        tooltip: {},
        dataset: {
            source: [
                ['product', 'NO2', 'SO2', 'NH3', 'HCL', 'Фармальдегід'],
                ['1.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006],
                ['2.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006],
                ['3.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006],
                ['4.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006],
                ['5.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006],
                ['6.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006],
                ['7.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006],
                ['8.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006],
                ['9.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006],
                ['10.12.2018', 0.03, 0.013, 0.02, 0.04, 0.006]
            ]
        },
        xAxis: {
          type: 'category',
          axisLabel: {
            rotate: 45,
            color: "#848e99"
          }
        },
        yAxis: {},
        // Declare several bar series, each will be mapped
        // to a column of dataset.source by default.
        series: [
          {type: 'bar'},
          {type: 'bar'},
          {type: 'bar'},
          {type: 'bar'},
          {type: 'bar'}
        ]
    };

    // use configuration item and data specified to show chart
    myChart2.setOption(option2);
  }



  /////////////////////////////////////////////////////////////////
  // Тестовая модаль
  /////////////////////////////////////////////////////////////////


  // $( "a, button" ).on( "click", function() {
  //   $('#modalTest').modal();
  // });


  // setTimeout("$('#modalTest').modal()", 5000);



  // Activate checkboxes
  $('.js-active-checkboxes').on('click', function() {
     $(this).parents('form').find('input[type=checkbox]').prop('checked', true);
  })

});


/////////////////////////////////////////////////////////////////
// FORM VALIDATION
/////////////////////////////////////////////////////////////////
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


