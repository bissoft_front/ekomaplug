

var map, infoBubble;
function init() {
  var mapCenter = new google.maps.LatLng(48.30978575, 37.17038329);
  map = new google.maps.Map(document.getElementById('map-main'), {
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // How zoomed in you want the map to start at (always required)
    zoom: 13,

    scrollwheel: false
  });

  // var marker = new google.maps.Marker({
  //   position: new google.maps.LatLng(48.30978575, 37.17038329)
  // });


  $(".b-map-infoblock").each(function() {
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(Number($(this).data('lat')), Number($(this).data('lng'))),
      map: map,
      title: $(this).data('title')
    });
  })

  var contentString =

    '<a class="b-map-bubble" href="#">' +
      '<div class="b-map-bubble__title">Звернення № 4561</div>' +
      '<div class="b-map-bubble__info">Накопичення сміття, не здійснюють утилізацію.</div>' +
    '</a>' ;


  infoBubble = new InfoBubble({
    minWidth: 300,
    minHeight: 100,
    content: contentString,
    shadowStyle: 1,
    padding: 12,
    arrowSize: 10,
    borderWidth: 1,
    arrowPosition: 30,
    borderRadius: 4
  });

  infoBubble.open(map, marker);


}
google.maps.event.addDomListener(window, 'load', init);
